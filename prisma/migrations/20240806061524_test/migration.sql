-- CreateExtension
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

-- CreateTable
CREATE TABLE "User" (
    "id" TEXT NOT NULL,
    "Nom" TEXT,
    "Prenom" TEXT,
    "Numero" TEXT,
    "Email" TEXT NOT NULL,
    "Picture" TEXT,
    "Mot_de_passe" TEXT NOT NULL,
    "CreatedAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "UpdatedAt" TIMESTAMP(3) NOT NULL
);

-- CreateTable
CREATE TABLE "Prompt" (
    "id" SERIAL NOT NULL,
    "Text" TEXT NOT NULL,
    "CreatedCount" INTEGER NOT NULL DEFAULT 0,
    "UpdatedCount" INTEGER NOT NULL DEFAULT 0,

    CONSTRAINT "Prompt_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Sentiment" (
    "id" SERIAL NOT NULL,
    "Label" TEXT NOT NULL,

    CONSTRAINT "Sentiment_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "PromptOnSentiment" (
    "idPromptOnSentiment" SERIAL NOT NULL,
    "idPrompt" INTEGER NOT NULL,
    "idSentiment" INTEGER NOT NULL,
    "type" TEXT NOT NULL,
    "CreatedAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "Author" TEXT NOT NULL,

    CONSTRAINT "PromptOnSentiment_pkey" PRIMARY KEY ("idPromptOnSentiment")
);

-- CreateIndex
CREATE UNIQUE INDEX "User_Email_key" ON "User"("Email");

-- CreateIndex
CREATE UNIQUE INDEX "Sentiment_Label_key" ON "Sentiment"("Label");
