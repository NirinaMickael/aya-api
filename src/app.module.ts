import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AppConfigModule } from './config/config.module';
import { PrismaModule } from './database/prisma.module';
import { UserModule } from './modules/user/user.module';
import { AuthModule } from './modules/auth/auth.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { v4 as uuidv4 } from 'uuid';
import { SentimentModule } from './modules/sentiment/sentiment.module';
import { PromptModule } from './modules/prompt/prompt.module';
import { PromptOnSentimentModule } from './modules/prompt-on-sentiment/prompt-on-sentiment.module';
import { MailerModule } from '@nestjs-modules/mailer';  
import { EjsAdapter } from '@nestjs-modules/mailer/dist/adapters/ejs.adapter';
import { AppConfigService } from './config/config.service';
@Module({
  imports: [AppConfigModule, PrismaModule, UserModule,
    AuthModule,
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, 'public'),
    }),
    SentimentModule,
    PromptModule,
    PromptOnSentimentModule,
    MailerModule.forRootAsync((
      
      {
        imports: [AppConfigModule],
        inject: [AppConfigService],
        useFactory : (configureService : AppConfigService) => (
          {
            transport: {
              host: configureService.mailHost,
              secure: true,
              port: 465,
              auth: {
                user: configureService.mailUser,
                pass: configureService.mailPass,
              },
            },
            defaults: {
              from:'"nest-modules" <modules@nestjs.com>',
            },
            template: {
              dir: process.cwd() + '/templates/',
              adapter: new EjsAdapter(),
              options: {
                strict: true,
              },
            },
          }
        )
      }
    ))
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor() {
    const s = uuidv4().split('-').at(-1).split('')
    console.log(s.filter((e:any)=>isNaN(e)).join('')+s.filter((e:any)=>!isNaN(e)).join(''))
  }
}
