export class Cursor {
    num_ligne:number;
    num_begin:number;
    orderBy : {
        col:string;
        ord:"asc" | "desc"
    };
    date: { start: Date, fin: Date }
    ids?:string[]
}   

