import { AbstractHttpAdapter } from '@nestjs/core';

import {
  Catch,
  HttpException,
  ArgumentsHost,
  ExceptionFilter,
  UnauthorizedException,
  NotFoundException,
} from '@nestjs/common';
import {
  PrismaClientRustPanicError,
  PrismaClientValidationError,
  PrismaClientKnownRequestError,
  PrismaClientUnknownRequestError,
  PrismaClientInitializationError,
} from '@prisma/client/runtime/library';

@Catch()
export class ErrorFilter implements ExceptionFilter {
  constructor(private readonly httpAdapterHost: AbstractHttpAdapter) {

  }
  catch(exception: any, host: ArgumentsHost): void {
    let errorMessage: any;
    let httpStatus: number;
    const httpAdapter = this.httpAdapterHost;
    const ctx = host.switchToHttp();
    console.log(exception)
    if (exception instanceof PrismaClientRustPanicError) {
      httpStatus = 400;
      errorMessage = exception.message;
    } else if (exception instanceof PrismaClientValidationError) {
      httpStatus = 422;
      errorMessage = exception.message;
    } else if (exception instanceof PrismaClientKnownRequestError) {
      httpStatus = 400;
      if (exception.code === 'P2002') {
        httpStatus=200;
        errorMessage = `Unique constraint failed on the fields: ${exception.meta.target? exception.meta.target[0] : 'Unknown'}`;
      }
      if (exception.code === 'P2025') {
        errorMessage = ` null constraint violation: ${exception.meta.target  ? exception.meta.target[0] : 'Unknown'}`;
      }
      if (exception.code === 'P2003') {
        errorMessage = `A foreign key constraint violation. ${exception.meta.target ? exception.meta.target[0] : 'Unknown'}`;
      }
    } else if (exception instanceof PrismaClientUnknownRequestError) {
      httpStatus = 400;
      errorMessage = exception.message;
    } else if (exception instanceof PrismaClientInitializationError) {
      httpStatus = 400;
      errorMessage = exception.message;
    }else if (exception instanceof NotFoundException) {
      httpStatus = 200;
      errorMessage = exception.message;
    }  
    else if (
      exception.statusCode &&
      exception.statusCode >= 400 &&
      exception.statusCode <= 499
    ) {
      httpStatus = exception.statusCode;
      errorMessage = exception.message;
    }else if (exception instanceof UnauthorizedException){
      httpStatus = 401
      errorMessage = "Unauthorized"
    }
    else {
      httpStatus = 500;
      errorMessage = exception.message
    }
    const errorResponse = {
      message: typeof (errorMessage) == "string" ? errorMessage.split("\n\n").at(-1) : errorMessage,
      success: false,
      data: null
    };
    httpAdapter.reply(ctx.getResponse(), errorResponse, httpStatus);
  }
}