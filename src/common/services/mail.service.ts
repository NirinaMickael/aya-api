import { MailerService } from "@nestjs-modules/mailer";
import { Injectable } from "@nestjs/common";

@Injectable()
export class MailService {
  constructor(private readonly mailerService: MailerService) {}
  async sendMail() {
    try {
      await this.mailerService.sendMail({
        to: 'andrainabaritiana@gmail.com',
        from: 'todisoanirinamickael@gmail.com',
        subject: 'Quotes', 
        text: '',
        html: '<p>How many programmers does it take to change a light bulb?',
      });
      return {
        success: true,
      };
    } catch (error) {
      return {
        success: false,
      };
    }
  }
}