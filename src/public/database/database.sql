
-- CreateTable
CREATE TABLE IF NOT EXISTS "User" (
    "Id_user" TEXT PRIMARY KEY NOT NULL,
    "Nom" TEXT,
    "Prenom" TEXT,
    "Numero" TEXT,
    "Email" TEXT NOT NULL,
    "Zone" TEXT,
    "Mot_de_passe" TEXT NOT NULL,
    "CreatedAt" DATE ,
    "UpdatedAt" DATE 
);

-- CreateTable
CREATE TABLE IF NOT EXISTS "Bien" (
    "Id_bien" TEXT PRIMARY KEY NOT NULL,
    "Type" TEXT,
    "Numero_serie" TEXT NOT NULL,
    "Numero_matricule" TEXT NOT NULL,
    "Marque" TEXT,
    "Detenteur"        TEXT, 
    "Localistion"      TEXT,
    "Acquisition"      TEXT,
    "Date_acquisition" TEXT,
    "Etat"             TEXT,  
    "Identifiant"      TEXT,
    "Is_Offline" TEXT,
    "CreatedAt" DATE ,
    "UpdatedAt" DATE 
);

-- CreateTable
CREATE TABLE IF NOT EXISTS "Ouvrier" (
    "Id_ouvrier" TEXT PRIMARY KEY NOT NULL,
    "Num_matricule" TEXT,
    "Nom" TEXT,
    "Prenom" TEXT,
    "Genre" TEXT,
    "Numero_cin" TEXT NOT NULL,
    "Date_cin" DATE,
    "Region" TEXT,
    "District" TEXT,
    "Commune" TEXT,
    "Subgratee" TEXT,
    "Pays" TEXT,
    "Lieu_cin" TEXT,
    "Lieu_naissance" TEXT,
    "Age" INTEGER,
    "Adresse" TEXT,
    "Site" TEXT,
    "Zone" TEXT,
    "Tribu" TEXT,
    "Is_Offline" TEXT,
    "Nombre_famille" INTEGER NOT NULL DEFAULT 0,
    "Nombre_femme" INTEGER NOT NULL DEFAULT 0,
    "CreatedAt" DATE ,
    "UpdatedAt" DATE 
);

-- CreateTable
CREATE TABLE IF NOT EXISTS "PresenceOuvrier" (
    "Id_presence_oeuvrier" TEXT PRIMARY KEY NOT NULL,
    "Id_ouvrier" TEXT NOT NULL,
    "Activite" TEXT,
    "Sous_activite" TEXT,
    "Tache" TEXT,
    "Id_Site" TEXT,
    "Rendement" TEXT,
    "Quantite_realise" TEXT,
    "Unite" TEXT,
    "Date"  DATE,
    "CreatedAt" DATE ,
    "UpdatedAt" DATE ,
    FOREIGN KEY ("Id_ouvrier") REFERENCES "Ouvrier"("Id_ouvrier")  ON UPDATE CASCADE  ON DELETE CASCADE
);

-- CreateTable
CREATE TABLE IF NOT EXISTS "PresenceBien" (
    "Id_presence_bien" TEXT PRIMARY KEY NOT NULL,
    "Id_bien" TEXT NOT NULL,
    "CreatedAt" DATE ,
    "Date"  DATE,
    "UpdatedAt" DATE ,
    FOREIGN KEY ("Id_bien") REFERENCES "Bien"("Id_bien")  ON UPDATE CASCADE  ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS "SavedData" (
  "id" TEXT PRIMARY KEY NOT NULL,
  "target" TEXT,
  "action_type" TEXT
);
-- CreateIndex
CREATE UNIQUE INDEX IF NOT EXISTS "User_Email_key" ON "User"("Email");

-- CreateIndex
CREATE UNIQUE INDEX IF NOT EXISTS "Bien_Numero_serie_key" ON "Bien"("Numero_serie");

-- CreateIndex
CREATE UNIQUE INDEX IF NOT EXISTS "Bien_Numero_matricule_key" ON "Bien"("Numero_matricule");

-- CreateIndex
-- CREATE UNIQUE INDEX IF NOT EXISTS "Ouvrier_Numero_cin_key" ON "Ouvrier"("Numero_cin");
