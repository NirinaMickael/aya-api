import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
export const swaggerOptions = new DocumentBuilder()
    .setTitle('attendance')
    .setDescription('Documentation')
    .setVersion('1.0')
    .addServer('http://localhost:3000/', 'Local environment')
    // .addServer('https://staging.yourapi.com/', 'Staging')
    // .addServer('https://production.yourapi.com/', 'Production')
    .addTag('Your API Tag')
    .build();

