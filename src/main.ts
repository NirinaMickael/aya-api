import { AbstractHttpAdapter, HttpAdapterHost, NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { AppConfigService } from './config/config.service';
import { ErrorFilter } from './common/middlewares';
import { SwaggerModule } from '@nestjs/swagger';
import { swaggerOptions } from './openapi/swagger/swagger.api';
import * as express from 'express';
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const appConfig = app.get(AppConfigService);
  app.setGlobalPrefix('api');
  app.use(express.json({ limit: '10mb' }))
  const adapterHost = app.get(HttpAdapterHost);
  const httpAdapter = adapterHost.httpAdapter;

  app.useGlobalFilters(new ErrorFilter(httpAdapter));
  app.enableCors({
    origin: "*",
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    preflightContinue: false,
    optionsSuccessStatus: 204,
    allowedHeaders:"*",
    credentials:true,
 })
  const document = SwaggerModule.createDocument(app, swaggerOptions);
  SwaggerModule.setup('api-docs', app, document);
  await app.listen(appConfig.port);
}
bootstrap();
