export default () => ({
  port: process.env.APP_PORT,
  mailHost : process.env.MAIL_HOST,
  mailUser : process.env.MAIL_USER,
  mailPass: process.env.MAIL_PASS,
  env: String(process.env.APP_ENV),
  frontUrl : process.env.FRONT_URL,
  jwtSecret:process.env.JWT_SECRET,
  exp:process.env.EXP,
});
