import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";

@Injectable()
export class AppConfigService {

    constructor(private configService: ConfigService) {
    }

    get env(): string {
        return this.configService.get<string>("env", {
            infer: true
        }) ?? " ";
    }
    get port(): number {
        
        return this.configService.get<number>("port", {
            infer: true
        });
    }
    get frontUrl(): string {
        return this.configService.get<string>("frontUrl", {
            infer: true
        }) ?? " ";
    }
    get mailHost() : string {
        return this.configService.get<string>("mailHost", {
            infer: true
        }) ?? " "
    }
    get mailUser() : string {
        return this.configService.get<string>("mailUser")
    }
    get mailPass() : string {
        return this.configService.get<string>("mailPass")
    }
}