import { HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { UserReponse } from './dto/user.schema';
import { PrismaService } from 'src/database/prisma.service';
import { CreateUserDto } from './dto';
import { UpdateUserDto } from './dto/user.dto';
import { Prisma } from '@prisma/client';
import { MailService } from 'src/common/services/mail.service';

@Injectable()
export class UserService {
    constructor(private prismaService: PrismaService , private mailService:MailService) { }
    async GetUser(id: string): Promise<UserReponse | null> {
        try {
            // const result = await this.mailService.sendMail();
            // console.log("result",result);
            
            // const user = await this.prismaService.user.findUnique({
            //     where: { id: id },
            //     select: {
            //         Mot_de_passe: false,
            //         Email: true,
            //         Nom: true,
            //         Prenom: true,
            //         id: true,
            //         CreatedAt: true,
            //         Numero: true
            //     }
            // })
            // if (!user) {
            //     throw new NotFoundException("user not found");
            // }
            // return {
            //     message: ``,
            //     data: user as UpdateUserDto,
            //     success: true
            // }
            const query = Prisma.sql`
            SELECT 
                u."id", u."Nom", u."Prenom",
                 u."Numero", u."Email",u."Picture"
            FROM 
                "User" u
            WHERE 
                u."id" = ${id}
          `;
          const user = await this.prismaService.$queryRaw(query);
          return {
            message: ``,
            data: user[0] as UpdateUserDto,
            success: true
        }
        } catch (error) {
            throw error
        }
    }
    async GetUserByEmail(email: string): Promise<UserReponse | null> {
        try {
            const user = await this.prismaService.user.findFirst(
                {
                    where: {
                        Email: email
                    }
                }
            )
            return {
                message: ``,
                data: user as UpdateUserDto,
                success: true
            }
        } catch (error) {
            throw error
        }
    }
    async GetAllUser(cursor: number): Promise<UserReponse | null> {
        
        try {
            const mycursor = cursor ?? 0
            console.log(mycursor)
            const user = await this.prismaService.user.findMany({
                take: 4,
                skip: mycursor,
                select: {
                    Mot_de_passe: false,
                    Email: true,
                    Nom: true,
                    Prenom: true,
                    id: true,
                    CreatedAt: true,
                    Numero: true
                }

            })
            if (!user) {
                throw new NotFoundException("No User found");
            }
            return {
                message: `list of users`,
                data: user as UpdateUserDto[],
                success: true
            }
        } catch (error) {
            throw error
        }
    }
    async CreateUser(userPayload: CreateUserDto): Promise<UserReponse | null> {
        console.log(userPayload);
        
        try {
            const user = await this.prismaService.user.create({
                data: userPayload,
                select: {
                    Mot_de_passe: false,
                    Email: true,
                    Nom: true,
                    Prenom: true,
                    id: true,
                    CreatedAt: true,
                    Numero: true
                }
            })
            console.log("user",user);
            
            return {
                message: `user with name : ${user.Nom} is created`,
                data: user as UpdateUserDto,
                success: true
            }
        } catch (error) {
            throw error
        }
    }

    // async UpdateUser(id: string, userPayload: UpdateUserDto): Promise<UserReponse | null> {
    //     try {
    //         const currentUser = await this.GetUser(id);
    //         const user = await this.prismaService.user.update({
    //             where: {
    //                 id:currentUser.data?.id
    //             },
    //             data: {
    //                 ...currentUser.data,
    //                 ...userPayload
    //             },
    //             select: {
    //                 Mot_de_passe: false,
    //                 Email: true,
    //                 Nom: true,
    //                 Prenom: true,
    //                 id: true,
    //                 CreatedAt: true,
    //                 Numero: true
    //             }
    //         })
    //         if (!user) {
    //             throw new NotFoundException("user not found");
    //         }
    //         return {
    //             message: `user with id : ${user.id} is updated`,
    //             data: user as UpdateUserDto,
    //             success: true
    //         }
    //     } catch (error) {
    //         throw error
    //     }
    // }

}
