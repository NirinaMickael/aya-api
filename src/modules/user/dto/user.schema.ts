import { Prisma } from "@prisma/client";
import { CreateUserDto, UpdateUserDto } from "./user.dto";

export type IUser = Prisma.UserCreateInput
export class UserReponse {
    success: boolean
    data: UpdateUserDto | UpdateUserDto[]
    message: string
    createdAt?: string | Date
    updatedAt?: string | Date
}