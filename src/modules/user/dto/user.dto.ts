
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, Length, MaxLength } from 'class-validator';
import { IUser } from './user.schema';

import { PartialType } from '@nestjs/mapped-types';
export class CreateUserDto implements IUser {
  id: string;
  @ApiProperty()
  @IsEmail()
  Email: string;

  @ApiProperty()
  @Length(6,15)
  Mot_de_passe: string;

  @ApiProperty()
  @Length(4,25)
  Nom: string; 

  @ApiProperty()
  @Length(4,25)
  Prenom: string;

  @ApiProperty()
  @Length(4,25)
  Numero: string;

  @ApiProperty()
  @Length(4,15)
  Zone: string;

  CreatedAt : Date
  UpdatedAt?: string | Date;
}

export class UpdateUserDto extends PartialType(CreateUserDto) {}