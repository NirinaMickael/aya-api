import { Global, Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { MailService } from 'src/common/services/mail.service';
@Global()
@Module({
  controllers: [UserController],
  providers: [UserService,MailService],
  exports:[UserService]
})
export class UserModule {}
