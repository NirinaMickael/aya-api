import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto, UserReponse } from './dto';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { UpdateUserDto } from './dto/user.dto';
@ApiTags("USER")
@Controller('user')
export class UserController {
    constructor(private userService: UserService) { }

    @Get(":id")
    @ApiOkResponse({ type: UserReponse })
    GetUser(@Param('id') id: string) {
        return this.userService.GetUser(id);
    }
    // @UseGuards(AuthGuard)
    @Post()
    @ApiOkResponse({ type: UserReponse })
    GetAllUser(@Body("cursor") cursor: number) {
        return this.userService.GetAllUser(cursor);
    }
    @Post("create")
    @ApiOkResponse({ type: UserReponse })
    CreateUser(@Body() userPayload: CreateUserDto) {
        return this.userService.CreateUser(userPayload)
    }
    // @Delete(":id")
    // @ApiOkResponse({ type: UserReponse })
    // DeleteUser(@Param('id') id: string) {
    //     return this.userService.DeleteUser(id);
    // }
    // @Patch(":id")
    // @ApiOkResponse({ type: UserReponse })
    // UpdateUser(@Param('id') id: string, @Body() userPayload: UpdateUserDto) {
    //     return this.userService.UpdateUser(id, userPayload);
    // }
}
