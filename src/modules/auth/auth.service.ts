import { BadRequestException, Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtService } from "@nestjs/jwt";
import * as bcrypt from "bcrypt"
import { CreateUserDto, UpdateUserDto } from '../user/dto/user.dto';
import { MailService } from 'src/common/services/mail.service';
@Injectable()
export class AuthService {

    constructor(
        private userService: UserService,
        private jwtService: JwtService,
        private mailService:MailService
    ) { }

    async validateUser(userPayload: UpdateUserDto) {
        try {
            const user = await this.userService.GetUserByEmail(userPayload.Email);
            if (!user.data) {
                throw new BadRequestException('User not found');
            }
            console.log(user.data)
            const isMatch: boolean = await bcrypt.compare(userPayload.Mot_de_passe, (user.data as CreateUserDto).Mot_de_passe);
            if (!isMatch) {
                throw new BadRequestException('Password does not match');
            }
            return user.data;
        } catch (error) {
            throw error
        }
    }
    async SignUp(userPayload: CreateUserDto) {
        try {
            const user = await this.userService.GetUserByEmail(userPayload.Email);
            if (user.data) {
                throw new BadRequestException("email already exists");
            }
            const hashedPassword = await bcrypt.hash(userPayload.Mot_de_passe, 10);
            const newUser: CreateUserDto = { ...userPayload, Mot_de_passe: hashedPassword };
            const userAdded = await this.userService.CreateUser(newUser);
            return userAdded
        } catch (error) {
            throw error
        }
    }

    async Login(userPayload: UpdateUserDto) {

        try {
            await this.mailService.sendMail();
            const user = await this.validateUser(userPayload);
            return {
                message: "",
                data: {
                    access_token: this.jwtService.sign(user)
                },
                success: true
            }
        } catch (error) {
            throw error;
        }
    }
}
