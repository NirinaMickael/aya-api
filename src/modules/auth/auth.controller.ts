import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateUserDto } from '../user/dto';
import { ApiTags } from '@nestjs/swagger';
import { UpdateUserDto } from '../user/dto/user.dto';
@ApiTags('AUTH')
@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService) { }
    @Post("login")
    Login(@Body() userPayload: UpdateUserDto) {
        return this.authService.Login(userPayload)
    }

    @Post("signup")
    SignUp(@Body() userPayload: CreateUserDto) {
        return this.authService.SignUp(userPayload)
    }

    // @Post()
    // ForgotPassword() {
    //     return this.authService.ForgotPassword();
    // }

}

