import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { JwtModule } from '@nestjs/jwt';
import configuration from 'src/config/configuration';
import { LocalStrategy } from './passport/strategy.service';
import { UserService } from '../user/user.service';
import { MailService } from 'src/common/services/mail.service';
@Module({
  imports: [
    JwtModule.register({
      global: true,
      secret: configuration().jwtSecret,
      signOptions: { expiresIn: configuration().exp },
    }),
  ],
  providers: [AuthService, UserService,MailService],
  controllers: [AuthController]
})
export class AuthModule { }
