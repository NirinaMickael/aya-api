import { Injectable } from '@nestjs/common';
import { CreateSentimentDto } from './dto/create-sentiment.dto';
import { UpdateSentimentDto } from './dto/update-sentiment.dto';
import { PrismaService } from 'src/database/prisma.service';

@Injectable()
export class SentimentService {
  constructor(private prisma : PrismaService){}
  async create(createSentimentDto: CreateSentimentDto | CreateSentimentDto[]) {

    return await this.prisma.sentiment.createMany({
      data:createSentimentDto
    });
  }

  async findAll() {
    return await this.prisma.sentiment.findMany();
  }

  findOne(id: number) {
    return `This action returns a #${id} sentiment`;
  }

  update(id: number, updateSentimentDto: UpdateSentimentDto) {
    return `This action updates a #${id} sentiment`;
  }

  remove(id: number) {
    return `This action removes a #${id} sentiment`;
  }
}
