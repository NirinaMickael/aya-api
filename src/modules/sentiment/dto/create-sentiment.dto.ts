import { ApiProperty } from "@nestjs/swagger";
import { Sentiment } from "../entities/sentiment.entity";

export class CreateSentimentDto implements Sentiment {
    @ApiProperty()
    Label: string;
}
