import { Injectable } from '@nestjs/common';
import { CreatePromptOnSentimentDto } from './dto/create-prompt-on-sentiment.dto';
import { UpdatePromptOnSentimentDto } from './dto/update-prompt-on-sentiment.dto';
import { PrismaService } from 'src/database/prisma.service';
import { Prisma } from '@prisma/client';
import { PromptService } from '../prompt/prompt.service';

@Injectable()
export class PromptOnSentimentService {
  constructor(private prisma: PrismaService, private promptService: PromptService) { }

  async create(data: CreatePromptOnSentimentDto[]) {
    const newPayload: any[] = [];
    try {
      await Promise.all(data.map(async (el) => {
        const obj: any = {};
        obj.type = el.type === "CREATE" ? 'CreatedCount' : 'UpdatedCount';
        obj.id = el.idPrompt;
        await this.promptService.update(obj);
  
        (el.idSentiment as any).forEach((s: any) => {
          newPayload.push({
            ...el,
            idSentiment: s
          });
        });
      }));
  
      return await this.prisma.promptOnSentiment.createMany({
        data: newPayload,
      });
    } catch (error) {
      return {
        data: null,
        error,
        success: false
      };
    }

  }

  async findAll(payload: Record<any, string>) {
    const authorEmail = 'auth0|669a529d69f488b44f857526'
    const query = Prisma.sql`
    SELECT 
        p."Text",pos."idPrompt",
        u."id", u."Nom", u."Prenom",
         u."Numero", u."Email",u."Picture",
        array_agg(pos."idSentiment") AS "sentiments"
    FROM 
        "PromptOnSentiment" pos
    JOIN 
        "User" u ON pos."Author" = u."id"
    JOIN 
        "Prompt" p ON pos."idPrompt" = p."id"
    WHERE 
        pos."type" =  ${payload.type} 
        AND u."id" = ${payload.id}
    GROUP BY 
        pos."idPrompt", pos."Author", p."Text", u."id", u."Nom", u."Prenom",
         u."Numero", u."Email",u."Picture",p."id";
  `;
  try {
    const results = await this.prisma.$queryRaw(query) as any[];
    console.log("results", results);
    const user = results[0];
    if (results.length === 0) {
      return {
        user, data: {
          sentiments: [],
          prompts: []
        }
      };
    }

    delete user.Text;
    delete user.sentiments;
    const data = results.map(row => ({
      Label: {
        text: row.Text,
        id: row.id
      },
      sentiments: row.sentiments,
    }));


    return {
      user, data: {
        sentiments: results.map(row => row.sentiments),
        prompts: results.map((row) => (
          {
            Text: row.Text,
            id: row.idPrompt,
          }
        ))
      }
    };
  } catch (error) {
    return {
      data: null,
      error,
      success: false
    };
  }
  }

  async findOne(idPrompt: number, idSentiment: number) {
    return ""
  }

  async update(idPrompt: number, idSentiment: number, data: UpdatePromptOnSentimentDto) {
    return ""
  }

  // async remove(idPrompt: number, idSentiment: number) {
  //   return this.prisma.promptOnSentiment.delete({
  //     where: { idPrompt_idSentiment: { idPrompt, idSentiment } },
  //   });
  // }

  async getLeaderboard(payload : Record<any,string>) {
    const offset = (+payload.page - 1) * +payload.pageSize;
    const query = `
      SELECT 
        u."id",
        u."Nom",
        u."Prenom",
        u."Email",
        u."Picture",
        CAST(COUNT(p."idPrompt") AS INT)  as "promptCount"
      FROM "PromptOnSentiment" p
      JOIN "User" u ON p."Author" = u."id"
      GROUP BY u."id", u."Nom", u."Prenom", u."Email", u."Picture"
      ORDER BY "promptCount" DESC
      LIMIT ${+payload.pageSize}
      OFFSET ${+payload.page};
      ;
    `;
    const countQuery = ` 
      SELECT CAST(COUNT(*)  AS INT ) AS nbr_lignes
      FROM  "Prompt";
    `
    try {
      
      const count  = await this.prisma.$queryRawUnsafe(countQuery);
      
      const results = await this.prisma.$queryRawUnsafe(query) as any;
      return {
        data: results.map((row,id)=>{
          const score  = Math.ceil((row.promptCount/count[0].nbr_lignes)*100);
          let  grade = ""
          if(score>0 &&  score<33){
            grade = "A"
          }else if(score>33 &&  score<67){
            grade = "B"
          }else{
            grade = "C"
          }
          return {
            ...row,
            ranking : id+1,
            score,
            grade 
          }
        }),
        success: true
      };
    } catch (error) {
      return {
        data: null,
        error,
        success: false
      };
    }
  }


}
