import { Module } from '@nestjs/common';
import { PromptOnSentimentService } from './prompt-on-sentiment.service';
import { PromptOnSentimentController } from './prompt-on-sentiment.controller';
import { PromptService } from '../prompt/prompt.service';

@Module({
  controllers: [PromptOnSentimentController],
  providers: [PromptOnSentimentService,PromptService],
})
export class PromptOnSentimentModule {}
