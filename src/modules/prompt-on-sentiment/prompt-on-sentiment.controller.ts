import { Controller, Get, Post, Body, Patch, Param, Delete, Put, Query } from '@nestjs/common';
import { PromptOnSentimentService } from './prompt-on-sentiment.service';
import { CreatePromptOnSentimentDto } from './dto/create-prompt-on-sentiment.dto';
import { UpdatePromptOnSentimentDto } from './dto/update-prompt-on-sentiment.dto';

@Controller('prompt-on-sentiment')
export class PromptOnSentimentController {
  constructor(private readonly promptOnSentimentService: PromptOnSentimentService) {}

  @Post()
  create(@Body() createPromptOnSentimentDto: CreatePromptOnSentimentDto[]) {
     return this.promptOnSentimentService.create(createPromptOnSentimentDto);
  }



   @Get("leader")
  getLeaderBoard(@Query() payload:Record<any,string>) {
     return this.promptOnSentimentService.getLeaderboard(payload);
  }
 
  @Get()
  findAll(@Query() payload:Record<any,string>) {
     return this.promptOnSentimentService.findAll(payload);
  }
 
  @Get(':idPrompt/:idSentiment')
  findOne(@Param('idPrompt') idPrompt: number, @Param('idSentiment') idSentiment: number) {
     return this.promptOnSentimentService.findOne(idPrompt, idSentiment);
  }
 
  @Put(':idPrompt/:idSentiment')
  update(@Param('idPrompt') idPrompt: number, @Param('idSentiment') idSentiment: number, @Body() updatePromptOnSentimentDto: UpdatePromptOnSentimentDto) {
     return this.promptOnSentimentService.update(idPrompt, idSentiment, updatePromptOnSentimentDto);
  }
 
  @Delete(':idPrompt/:idSentiment')
  remove(@Param('idPrompt') idPrompt: number, @Param('idSentiment') idSentiment: number) {
     //return this.promptOnSentimentService.remove(idPrompt, idSentiment);
  }
}
