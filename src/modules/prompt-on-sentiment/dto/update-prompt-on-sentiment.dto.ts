import { PartialType } from '@nestjs/swagger';
import { CreatePromptOnSentimentDto } from './create-prompt-on-sentiment.dto';

export class UpdatePromptOnSentimentDto extends PartialType(CreatePromptOnSentimentDto) {}
