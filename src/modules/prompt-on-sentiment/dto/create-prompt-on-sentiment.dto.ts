import { Prisma } from "@prisma/client";
import { PromptOnSentiment } from "../entities/prompt-on-sentiment.entity";
import { ApiProperty } from "@nestjs/swagger";

export class CreatePromptOnSentimentDto implements PromptOnSentiment {
    @ApiProperty()
    type: string;
    @ApiProperty()
    idPrompt: number;
    @ApiProperty()
    idSentiment: number;
    @ApiProperty()
    Author: string;
}
