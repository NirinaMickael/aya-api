import { Injectable } from '@nestjs/common';
import { CreatePromptDto } from './dto/create-prompt.dto';
import { UpdatePromptDto } from './dto/update-prompt.dto';
import { PrismaService } from 'src/database/prisma.service';
import * as fs from 'fs';
import * as path from 'path';
import { Prisma } from '@prisma/client';
@Injectable()
export class PromptService {
  constructor(private prisma:PrismaService){}
  async create(file: string) {
    const configPath = path.resolve(__dirname, `../../public/public/${file}.json`);
    const fileContent = fs.readFileSync(configPath, 'utf-8');
    const prompts = JSON.parse(fileContent) as CreatePromptDto[];
    const values = prompts.map(data => data.text);
    const query = `
      INSERT INTO "Prompt" ("Text")
      VALUES ${values.map((_, index) => `($${index + 1})`).join(',')}
    `;
    return await this.prisma.$executeRawUnsafe(query, ...values);
  }
  async findAll(payload: Record<string, any>) {
    var query;
    const skip = +payload.skip || 0;
    const take = +payload.take || 10;
    if(payload.type === "UpdatedCount"){
      query = Prisma.sql`
      SELECT *
        FROM "Prompt"
        ORDER BY ${payload.type} ASC , LENGTH("Text") DESC
        OFFSET ${skip}
        LIMIT ${take}
      `
    }else{
      query = Prisma.sql`
      SELECT *
        FROM "Prompt"
        ORDER BY ${payload.type} DESC 
        OFFSET ${skip}
        LIMIT ${take}
      `
    }

    
    try {
      let results = await this.prisma.$queryRaw<
        { id: number; Text: string; CreatedCount: number; UpdatedCount: number }[]
      >(query);
      results = [results[Math.floor(Math.random()*10)]]
      return {
        prompts:results,
        cursor :skip
      };
    } catch (error) {
      return {
        data: null,
        error,
        success: false
      };
    }
  }

  async findOne(id: number) {
    return await this.prisma.prompt.findFirst(
      {
        where : {
          id:id
        }
      }
    );
  }

  async update(payload: Record<string, any>) {
    try {
      const currentPrompt = await this.findOne(+payload.id);
      const newCount = currentPrompt[payload.type]+1;
      const result = await this.prisma.prompt.update({
        where: { id: +payload.id },
        data: {
          [payload.type]: newCount,
        },
      });
      return result;
    } catch (error) {
      return {
        data: null,
        error,
        success: false
      };
    }


  }

  remove(id: number) {
    return `This action removes a #${id} prompt`;
  }
}
