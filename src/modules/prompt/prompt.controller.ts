import { Controller, Get, Post, Body, Patch, Param, Delete, Query, Put } from '@nestjs/common';
import { PromptService } from './prompt.service';
import { CreatePromptDto } from './dto/create-prompt.dto';

@Controller('prompt')
export class PromptController {
  constructor(private readonly promptService: PromptService) {}

  @Get(":file")
  create(@Param("file") file: string) {
    return this.promptService.create(file);
  }

  @Get()
  findAll(@Query() query: Record<string, any>) {
    return this.promptService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.promptService.findOne(+id);
  }

  @Put('')
  update(@Query() query: Record<string, any>) {
    return this.promptService.update(query);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.promptService.remove(+id);
  }
}
